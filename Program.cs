﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Linq;

namespace NotebookApp
{
    class Program
    {
      public static List<Note> notes = new List<Note>();

        static void Main(string[] args)
        {
            Console.Clear();

            while (1 == 1)
            {
                Console.WriteLine($" Добро пожаловать в записную книжку версии 2.1! \r\n Создать запсиь - создать" +
                    $" \r\n Посмотреть последнюю запись - последнюю  \r\n Удалить запись - удалить" +
                    $" \r\n Отредактировать запись - отредактировать по айди \r\n Посмотреть все записи - все " +
                    $" \r\n Чтобы просмотреть краткую ниформацию всех записей - кратко " +
                    $" \r\n Удалить запись по айди - айди \r\n Чтобы завершить программу - выйти ");

                string answer = Console.ReadLine();

                while (answer == "")
                {
                    Console.WriteLine("Введите слово!"); string answer2 = Console.ReadLine();
                    if (answer2 != "") { answer = answer2; break; } else continue;
                }
                
                switch (answer)//основное меню
                {
                    case "создать":
                        Console.Clear();
                        Console.WriteLine("создать запись");
                        CreateNewNote();
                        Console.Clear();
                        break;
                    case "последнюю": 
                        Console.Clear();
                        if (notes.Count == 0) //Проверка на пустоту
                        {
                            Console.WriteLine("Адресная книга пуста! \r\nНажмите на кнопку для продолжения");
                            Console.ReadKey();
                            Console.Clear();
                            continue ;
                        }
                        else {Console.WriteLine("посмотреть полседнюю запись");
                        ReadNote(Program.notes.Last()); }
                        Console.WriteLine("Нажмите на кнопку для продолжения");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case "удалить":
                        Console.Clear();
                        Console.WriteLine("удалить запись");
                        DeleteNote();
                        break;
                    case "отредактировать":
                        Console.Clear();
                        Console.WriteLine("отредактировать запись");
                        EditNote();
                        break;
                    case "все":
                        Console.Clear();
                        Console.WriteLine("просмотреть все записи");
                        ShowAllNotes();
                        break;
                    case "кратко":
                        Console.Clear();
                        Console.WriteLine("просмотреть краткую ниформацию всех записей");
                        ShowAllNotes2();
                        break;
                    case "айди":
                        Console.Clear();
                        Console.WriteLine("Удалить запись по айди");
                        DeleteNote2();
                        break;
                    case "выйти":
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine(" Вы ввели что-то не то! Нажмите на кнопку для продолжения");
                        Console.ReadKey();
                        Console.Clear();
                        break;
                }
            }
            
        }
       
      public  static void CreateNewNote() //Создание новой записи
        {
            Note note = new Note();
            note.id = notes.Count + 1;
            // здесь у меня ошибка связанная с id
            // ОНА БУДЕТ ИСПРАВЛЕНА В СЛЕДУЮЩЕЙ ВЕРСИИ 
            // (я создаю 3 новых записи, удаляю первую и создаю новую - у 2 и 3 объекта id=3 )
            // реализация сеттеров и геттеров также будет в следующей версии 
            Console.WriteLine("Введите имя");
            note.name = Console.ReadLine();
            while (note.name == "") // обязательное поле
            {
                Console.WriteLine("Введите имя!");
                string name2 = Console.ReadLine();
                if (name2 != "") { note.name = name2; break; }
                else continue;
            }
            Console.WriteLine("Введите фамилию");
            note.surname = Console.ReadLine();
            while (note.surname == "") // обязательное поле
            {
                Console.WriteLine("Введите фамилию!");
                string surname2 = Console.ReadLine();
                if (surname2 != "") { note.surname = surname2; break; }
                else continue;
            }
            
            Console.WriteLine("Введите отчество");
            note.last_name = Console.ReadLine();

            Console.WriteLine("Введите номер (только цифры)");
            int.TryParse(Console.ReadLine(), out note.number);
            while (note.number == 0) // обязательное поле 
            {
                Console.WriteLine("Введите номер! ТОЛЬКО ЦИФРЫ");
                int number2;
                int.TryParse(Console.ReadLine(), out number2); 
                if (number2 != 0) { note.number = number2; break; } // ТОЛЬКО ЦИФРЫ
                else continue;
            }

            Console.WriteLine("Введите страну");
            note.country = Console.ReadLine(); // обязательное поле
            while (note.country == "")
            {
                Console.WriteLine("Введите страну!");
                string country2 = Console.ReadLine();
                if (country2 != "") { note.country = country2; break; }
                else continue;
            }
            Console.WriteLine("Введите дату др");
            note.date_of_bd = Console.ReadLine();
            Console.WriteLine("Введите организацию");
            note.organisation = Console.ReadLine();
            Console.WriteLine("Введите должность");
            note.position = Console.ReadLine();
            Console.WriteLine("Введите доп заметки");
            note.other_notes = Console.ReadLine();
            notes.Add(note);
        }
        
        static void EditNote() //Редактирование ранее созданной записи по айди
        {
            if (notes.Count == 0) //Проверка на пустоту
            {
                Console.WriteLine("Адресная книга пуста! \r\nНажмите на кнопку для продолжения");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            Console.WriteLine("Кого вы хотите отредактировать? Введите id (только цифры от 1)");

           
            int.TryParse(Console.ReadLine(), out int id2);

            while (id2 == 0) // обязательное поле 
            {
                Console.WriteLine("Введите id! ТОЛЬКО ЦИФРЫ от 1");
                int id3;
                int.TryParse(Console.ReadLine(), out id3);
                if (id3 != 0) { id2 = id3; break; } // ТОЛЬКО ЦИФРЫ
                else continue;
            }

            Note note = notes.FirstOrDefault(x => x.id == id2);

            if (note == null)
            {
                Console.WriteLine("Человек не найден! Нажмите кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            Console.WriteLine("Вы отредактировать эту запись? (Y/N)");
            ReadNote(note);

            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.Clear();
                Console.WriteLine($"\r\n Какое поле отредактировать? \r\n имя \r\n фамилия \r\n отчество \r\n номер \r\n страна \r\n дата_др \r\n организация \r\n позиция \r\n заметки") ;
                string s = Console.ReadLine();
                switch (s)
                {
                    case "имя":
                        Console.WriteLine("Введите новое имя!");
                        string newname = Console.ReadLine();
                        while (newname == "") // обязательное поле
                        {
                            Console.WriteLine("Введите имя!");
                            string newname2 = Console.ReadLine();
                            if (newname2 != "") { note.name = newname2; break; }
                            else continue;
                        }
                        note.name = newname;
                        Console.Clear();
                        break;

                    case "фамилия":
                        Console.WriteLine("Введите новую фамилию!");
                        string newsurname = Console.ReadLine();
                        while (newsurname == "") // обязательное поле
                        {
                            Console.WriteLine("Введите фамилию!");
                            string newsurname2 = Console.ReadLine();
                            if (newsurname2 != "") { note.surname = newsurname2; break; }
                            else continue;
                        }
                        note.surname = newsurname;
                        Console.Clear();
                        break;
                    case "отчество":
                        Console.WriteLine("Введите отчество!");
                        string newlast_name = Console.ReadLine();
                        note.last_name = newlast_name;
                        Console.Clear();
                        break;
                    case "номер":
                        Console.WriteLine("Введите новый номер!");
                        int.TryParse(Console.ReadLine(), out int newnumber);

                        while (newnumber == 0) // обязательное поле 
                        {
                            Console.WriteLine("Введите номер! ТОЛЬКО ЦИФРЫ от 1");                            
                            int.TryParse(Console.ReadLine(), out int newnumber2);
                            if (newnumber2 != 0) { note.number = newnumber2; break; } // ТОЛЬКО ЦИФРЫ
                            else continue;
                        }
                        Console.Clear();
                        break;

                    case "страна":
                        Console.WriteLine("Введите новую страну!");
                        string newcountry = Console.ReadLine();
                        while (newcountry == "") // обязательное поле
                        {
                            Console.WriteLine("Введите страну!");
                            string newcountry2 = Console.ReadLine();
                            if (newcountry2 != "") { note.name = newcountry2; break; }
                            else continue;
                        }
                        note.country = newcountry;
                        Console.Clear();
                        break;
                    case "дата_др":
                        Console.WriteLine("Введите новую дату!");
                        string newdate = Console.ReadLine();
                        note.date_of_bd = newdate;
                        Console.Clear();
                        break;
                    case "организация":
                        Console.WriteLine("Введите организацию!");
                        string neworganisation = Console.ReadLine();
                        note.organisation = neworganisation;
                        Console.Clear();
                        break;
                    case "позиция":
                        Console.WriteLine("Введите позицию!");
                        string newposition = Console.ReadLine();
                        note.position = newposition;
                        Console.Clear();
                        break;
                    case "заметки":
                        Console.WriteLine("Введите заметки!");
                        string newnotes = Console.ReadLine();
                        note.other_notes = newnotes;
                        Console.Clear();
                        break;
                    default:
                        Console.WriteLine("Вы ввели что-то не то! Нажмите на кнопку для продолжения");
                        Console.ReadKey();
                        Console.Clear();
                        break;                        
                }

                return;
            }
            else
            {
                Console.WriteLine("Редактирование не выполнено! Нажмите кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
        }
        static void DeleteNote() //Удаление ранее созданной записи по имени
        {
            if (notes.Count == 0) //Проверка на пустоту
            {
                Console.WriteLine("Адресная книга пуста! \r\nНажмите на кнопку для продолжения");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            Console.WriteLine("Кого вы хотите удалить?");
            string firstName = Console.ReadLine();
            Note note = notes.FirstOrDefault(x => x.name.ToLower() == firstName.ToLower());
            Console.Clear();

            if (note == null)
            {
                Console.WriteLine("Человек не найден! Нажмите кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }

            Console.WriteLine("Вы уверены что хотите вычеркнуть этого человека из вашей жизни? (Y/N)");
            ReadNote(note);
            
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                notes.Remove(note);
                Console.Clear();
                Console.WriteLine("Всё, вычеркнули! Нажмите кнопку чтобы продолжить ");
                Console.ReadKey();
                Console.Clear();
            }
            else
            {
                Console.WriteLine("Удаление не выполнено! Нажмите кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
            }
        }
       private static void ReadNote(Note note) //Просмотр созданной записи
       {
            Console.WriteLine($"id-{note.id}");
            Console.WriteLine($"имя-{note.name}");
            Console.WriteLine($"фамилия-{note.surname}");
            Console.WriteLine($"отчество-{note.last_name}");
            Console.WriteLine($"номер-{note.number}");
            Console.WriteLine($"страна-{note.country}");
            Console.WriteLine($"дата др-{note.date_of_bd}");
            Console.WriteLine($"организация-{note.organisation}");
            Console.WriteLine($"должность-{note.position}");
            Console.WriteLine($"остальное-{note.other_notes}");
            Console.WriteLine("--------------------------");
       }
        private static void ReadNote2(Note note) //Просмотр краткой информации всех записей
        {
            Console.WriteLine($"имя-{note.name}");
            Console.WriteLine($"фамилия-{note.surname}");
            Console.WriteLine($"номер-{note.number}");
            Console.WriteLine("--------------------------");
        }
        static void ShowAllNotes() //Просмотр всех созданных записей в общем списке
        {
            if (notes.Count == 0) //Проверка на пустоту
            {
                Console.WriteLine("Адресная книга пуста! \r\nНажмите на кнопку для продолжения");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            Console.WriteLine("Вот всё что есть:\n");
            foreach (var note in notes)
            {
                ReadNote(note);
            }
            Console.WriteLine("\nНажмите кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }

        static void ShowAllNotes2() //Просмотр всех созданных записей в общем списке с краткой информацией
        {
            if (notes.Count == 0) //Проверка на пустоту
            {
                Console.WriteLine("Адресная книга пуста! \r\nНажмите на кнопку для продолжения");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            Console.WriteLine("Вот всё что есть:\n");
            foreach (var note in notes)
            {
                ReadNote2(note);
            }
            Console.WriteLine("\nНажмите кнопку чтобы продолжить");
            Console.ReadKey();
            Console.Clear();
        }
        static void DeleteNote2() //Удаление ранее созданной записи по айди
        {
            if (notes.Count == 0) //Проверка на пустоту
            {
                Console.WriteLine("Адресная книга пуста! \r\nНажмите на кнопку для продолжения");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            Console.WriteLine("Кого вы хотите удалить? Введите id (только цифры от 1)");

            int id2;

            int.TryParse(Console.ReadLine(), out id2);

            while (id2 == 0) // обязательное поле 
            {
                Console.WriteLine("Введите id! ТОЛЬКО ЦИФРЫ от 1");
                int id3;
                int.TryParse(Console.ReadLine(), out id3);
                if (id3 != 0) { id2 = id3; break; } // ТОЛЬКО ЦИФРЫ
                else continue;
            }

            Note note = notes.FirstOrDefault(x => x.id == id2);
            
            if (note == null)
            {
                Console.WriteLine("Человек не найден! Нажмите кнопку чтобы продолжить");
                Console.ReadKey();
                Console.Clear();
                return;
            }
            
            Console.WriteLine("Вы уверены что хотите вычеркнуть этого человека из вашей жизни? (Y/N)");
            ReadNote(note);

            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                notes.Remove(note);
                Console.Clear();
                Console.WriteLine("Всё, вычеркнули! Нажмите кнопку чтобы продолжить ");
                Console.ReadKey();
                Console.Clear();
            }
            else 
            { Console.WriteLine("Удаление не выполнено! Нажмите кнопку чтобы продолжить");
              Console.ReadKey();
              Console.Clear();
            }
        }

    }
}
